angular.module("app").controller('ProcurementController', function($scope, $location, $window, $localStorage, ProductService){


   var pricing_query = {};
   pricing_query.city_id = 1;

   $scope.pricing_list = {};
   $scope.sub_categories = {};
   $scope.markets = {};
   $scope.vendors = {};
   $scope.cartList = {};

    $scope.set_sub_category =  function(sub_category){
        pricing_query.sub_cat_id = sub_category.id;
    };

   	$scope.get_sub_categories = function(){
   		delete $scope.sub_categories
   		ProductService.get_all_subcategories(pricing_query.category_id).success(function(data){
   			$scope.sub_categories = data;
   		});
   	};

   	$scope.get_pricing = function(){
   		var pricing_query = $scope.pricing_query;
   		if(!pricing_query.city_id || !pricing_query.sub_cat_id){
   			alert("Select a city , Category, Sub Category to get prices");
   			return;
   		}
   		ProductService.get_prices(pricing_query).success(function(data){
   			$scope.pricing_list = data;
   		})
      delete $scope.markets
      ProductService.get_all_markets().success(function(data){
        $scope.markets = data;
      });
   	};

   	$scope.update_pricing = function(pricing){  
        ProductService.update_pricing(pricing).success(function(data){
   			alert("updated "+pricing.product.name);
   		});
   	};

    $scope.get_vendors = function(marketSelected){    
      delete $scope.vendors    
      ProductService.get_all_vendors(marketSelected).success(function(data){      
        $scope.vendors = data;      
      });    
    };

    $scope.addToCart = function(pricing, orderQty, marketSelected, vendorSelected){    
      var cart = new Object();
      cart.sku_code = pricing.product.id;
      cart.order_qty = orderQty;
      cart.sourcing_mkt_id = marketSelected;
      cart.vendor_id = vendorSelected;
      if(orderQty != null && marketSelected != null && vendorSelected != null){        
        ProductService.addToCart(cart).success(function(data){
            alert("Added to cart");
        });
      }else{
        alert("Please fill in the quantity procured and select market and vendor");
      }
    };

    $scope.placeOrder = function(cartList){
      if(cartList.length != 0){
        ProductService.placeProcurementOrder(cartList).success(function(data){
          alert("Order Placed!!!")
          delete $scope.cartList
        });
      }else{
        alert("Your cart is empty!!! please add items to cart")
      }
    };

    $scope.getAllCart = function(){
      ProductService.getAllCart().success(function(data){
        $scope.cartList = data        
      });
    };

    $scope.removeFromCart = function(cart){        
      ProductService.removeFromCart(cart).success(function(data){
          ProductService.getAllCart().success(function(data){
            $scope.cartList = data        
          });
          alert("Removed from cart");          
      });      
    };
});