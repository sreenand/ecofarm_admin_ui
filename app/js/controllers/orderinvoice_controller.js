angular.module("app").controller('OrderInvoiceController', function($scope, $routeParams, 
	$window, $location, $localStorage, OrdersSerivce) {

   $scope.Math = Math;
   var order_id = $routeParams.order_id;
   if(order_id == null){
   	   alert("Please provide a valid order id");
   	   $location.path("/home");
   	   return;
   }

   $('#panel-heading').css({'display':'none'});
   $scope.order = {};
   $scope.show_invoice = true;
   OrdersSerivce.get_order_by_id(order_id).success(function(order){      
   	    $scope.order = order;

          for(index in $scope.order.orderItems){         
            $scope.order.orderItems[index].product.localLanguageMap = JSON.parse(
               $scope.order.orderItems[index].product.localNames);
          }
   });
});