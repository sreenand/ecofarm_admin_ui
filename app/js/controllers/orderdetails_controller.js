angular.module("app").controller('OrderDetailsController', function($scope, $routeParams, $window, $location, $localStorage, OrdersSerivce) {

   var order_id = $routeParams.order_id;


   if(order_id == null){
   	   alert("Please provide a valid order id");
   	   $location.path("/home");
   	   return;
   }
  $scope.order = {};

  $scope.change_status = function(orderItem){
    if(orderItem.quantityProcured >= orderItem.quantity){
      orderItem.fulfillmentStatus = "FULFILLED";
    } else if(orderItem.quantityProcured > 0){
      orderItem.fulfillmentStatus = "PARTIALLY_FULFILLED";
    } else{
      orderItem.fulfillmentStatus = "NOT_FULFILLED";
    }
    orderItem.status = "PACKED";
  };

  $scope.change_order_status = function(){

    if($scope.order.status == 'DELIVERED' || 
             $scope.order.status == 'DISPATCHED' || $scope.order.status == 'RETURNED'){
      for(index in $scope.order.orderItems){
         $scope.order.orderItems[index].status = $scope.order.status;
      }
    }
  };

   $scope.calculate_amount = function(){

     orderAmount = 0;
     orderItems = $scope.order.orderItems;
     for(index=0; index < orderItems.length; index++){

     	if(orderItems[index].quantityProcured != null && orderItems[index].priceOfferedPerUnit){
     		orderItems[index].pricePerUnit = orderItems[index].priceOfferedPerUnit;
     		orderAmount = orderAmount + (orderItems[index].quantityProcured * orderItems[index].priceOfferedPerUnit);
     		if(orderItems[index].discount)
     		  orderAmount = orderAmount - orderItems[index].discount;
          orderAmount = Math.ceil(orderAmount);
     	} else{
     		alert("Please fill the price offered details before calculating the amount");
     		return;
     	}
     	
     }
     $scope.order.priceCalculated = orderAmount;
     $scope.order.pricePromised = orderAmount;
   };

    $scope.update_order_details = function(){
      OrdersSerivce.update_order($scope.order).success(function(order){
        $scope.order = order;
        alert("The Order Details have been updated Successfully");
      });

    };


    $scope.populate_prices = function(){
      OrdersSerivce.populate_prices($scope.order).success(function(data){

          // data.sort(function(a, b){
          // var keyA = a.id,
          //     keyB = b.id;
          // // Compare the 2 dates
          // if(keyA < keyB) return -1;
          // if(keyA > keyB) return 1;
          // return 0;});

        var orderItems = $scope.order.orderItems;
        for( index in orderItems){
           for(index2 in data){
            if(orderItems[index].id == data[index2].id){
              orderItems[index].pricePerUnit = data[index2].pricePerUnit;
              orderItems[index].priceOfferedPerUnit = data[index2].priceOfferedPerUnit;
              break;
            }
           }            
        }
        alert("The Price details are populated");
      });

    };

   $scope.go_to_invoice_page = function(){

     if($scope.order.status === "PACKED" && $scope.order.fulfillmentStatus != "NOT_FULFILLED"){
        $scope.order.status = "DISPATCHED";
        $scope.change_order_status();
     } 
     OrdersSerivce.update_order($scope.order).success(function(order){
          $window.open('/#/orders/'+order_id+'/invoice', 'C-Sharpcorner', 'width=1000,height=800');
     });
   };

   OrdersSerivce.get_order_by_id(order_id).success(function(order){
        for(i=0;i<order.orderItems.length;i++){
           if(order.orderItems[i].quantityProcured == null){
              order.orderItems[i].quantityProcured = order.orderItems[i].quantity;
           }

           if(order.orderItems[i].priceOfferedPerUnit == null){
              order.orderItems[i].priceOfferedPerUnit = order.orderItems[i].pricePerUnit;
           } 
        }
        $scope.order = order;
   });
});