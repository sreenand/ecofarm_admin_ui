angular.module("app").controller('ProcOrderDetailsController', function($scope, $routeParams, $window, $location, $localStorage, PurchaseOrderSerivce) {

   var order_id = $routeParams.order_id;


   if(order_id == null){
   	   alert("Please provide a valid order id");
   	   $location.path("/home");
   	   return;
   }
  $scope.order = {};

  $scope.change_order_status = function(status){
      for(index in $scope.order){
         $scope.order[index].purchaseOrder.status = status;         
      }
      PurchaseOrderSerivce.update_status($scope.order[0].po_id, status).success(function(order){
                  
      });
  };

  $scope.update_order_item_details = function(){
    PurchaseOrderSerivce.update_order($scope.order).success(function(order){
      $scope.order = order;
        alert("The Order Details have been updated Successfully");
    });
  };

  PurchaseOrderSerivce.get_order_by_id(order_id).success(function(order){      
      $scope.order = order;        
  });
});