angular.module("app").controller('ProcurementOrdersController', function($scope, $location, $window, $localStorage, 
  $templateCache, PurchaseOrderSerivce, $q) {


var max_count = 100;
var count = 0;
var limit = 10;
var offset = 0;
var loading_orders = false;

$scope.freq_list = [];



   var order_query = {

      status: "PLACED,UNDER_PROCUREMENT,PROCURED,RECEIVED,RETURNED,CANCELLED,CLOSED",
      city_id: 1,
      from_date: new Date(),
      to_date: new Date(),
      limit: 10,
      offset: count,
      max_count: max_count,
      selected_orders: 0
   }


   order_query.from_date.setHours("0");
   order_query.from_date.setMinutes("0");
   order_query.to_date.setHours("23");
   order_query.to_date.setMinutes("59");
   order_query.to_date.setSeconds("59");
   order_query.from_date.setSeconds("00");

   
   if($localStorage.order_query){
      $scope.order_query = $localStorage.order_query
      $scope.order_query.from_date = new Date($localStorage.order_query.from_date);
      $scope.order_query.to_date = new Date($localStorage.order_query.to_date);
      order_query = $scope.order_query;
   } else{
     $scope.order_query = order_query;
     $scope.orders = [];
     $scope.select_all = false;
     $localStorage.order_query = order_query;

   }

   $scope.orders = [];
   $scope.loading_orders = loading_orders;
   $scope.order_query.selected_orders = 0;

   
   
   if($localStorage.user == null){
     $location.path('/#/login');
     return;
   }

   $scope.get_next_orders_page = function(){

      try{

         if(max_count > count && !$scope.loading_orders){ 
            $scope.loading_orders = true;
            order_query.offset = count;
               PurchaseOrderSerivce.get_orders(order_query).success(function(next_page_orders){
                  if(next_page_orders.count > 0 ){
                     $scope.orders = $scope.orders.concat(next_page_orders.orders);
                     count = count + next_page_orders.orders.length;
                     max_count = next_page_orders.count; 
                     $scope.loading_orders =   false;
                  } else{
                     max_count = 0;
                     $scope.loading_orders = true;
                  }           
               })
            
         } else{
            $scope.loading_orders = true;
            return;
         }

      } catch(err){
         $scope.loading_orders = true;
      }

   }.bind(this);

   $scope.activate_order_search =  function(){
       if(order_query.from_date && order_query.to_date){  
         $scope.orders = [];
         $scope.loading_orders = false;
         max_count = 100000;
         $scope.order_query.selected_orders = 0;
         count = 0;
         offset = 0;
         $scope.select_all = false;
         $scope.$emit('query_again');
       } else {
          alert("Please Select a from and to time"); 
       }
   };

   $scope.go_to_order_details = function(order_id){
      $localStorage.order_query = $scope.order_query;
      $location.path('/purchase_order_details/'+order_id);      
   };

   $scope.select_order = function(order){
      if(order.selected)
        $scope.order_query.selected_orders += 1;
      else
        $scope.order_query.selected_orders -= 1;
   };

   $scope.select_all_orders = function(){
      

      if($scope.select_all){
       for(index in $scope.orders){
          $scope.orders[index].selected = true;
       }
       $scope.order_query.selected_orders = $scope.orders.length;
     } else{
        for(index in $scope.orders){
          delete $scope.orders[index].selected;
       }
      $scope.order_query.selected_orders = 0;
     }

   };


   $scope.change_status = function(){

     var selected_order_ids = [];

     for(index in $scope.orders){
      if($scope.orders[index].selected)
        selected_order_ids .push($scope.orders[index].id);     
     }

     if(selected_order_ids.length == 0){
      alert("Please select atleast one order for changing status");
      return;
     }
     var selected_ids = selected_order_ids.toString();
     
     PurchaseOrderSerivce.update_status(selected_ids, order_query.status).success(function(){
        alert("Orders Status has been updated");
     });

      $scope.orders = [];
      $scope.loading_orders = false;
      $scope.order_query.selected_orders = 0;
      max_count = 100000;
      count = 0;
      $scope.select_all = false;
      $scope.$emit('query_again');

   };

});
