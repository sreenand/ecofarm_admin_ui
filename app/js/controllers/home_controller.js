angular.module("app").controller('HomeController', function($scope, $location,$window, $localStorage, AuthenticationService) {
  $scope.title = "Home";
  $scope.message = "Mouse Over these images to see a directive at work";

   if($localStorage.user == null){
     $location.path('/login');
   }

  $scope.user = $localStorage.user;
  var onLogoutSuccess = function(response) {
    $location.path('/login');
  };

  $scope.logout = function() {
    AuthenticationService.logout().success(onLogoutSuccess);
  };

  $scope.launch_tab = function(relative_path){
    $window.open("/#/" + relative_path);
  }
});
