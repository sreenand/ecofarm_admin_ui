angular.module("app").controller('StoresController', function($scope, $location, $window, 
	$localStorage, $templateCache, StoreService, $q) {


  var placeSearch, autocomplete;
  $scope.store_query = {};
  $scope.store_query.limit = 200;
  $scope.store_query.offset = 0;
  $scope.store_query.city_id = 1;
  $scope.store_query.locality_id = 0;
  $scope.entered_locality = {};
  $scope.create_user = true;
  $scope.sales_users = {}

  var city_to_id_map = {
    'Coimbatore':1
  }

  $scope.stores = [];
  $scope.selectedStore = {};

  var componentForm = {
  	sublocality_level_2: 'long_name', // sub locality, street etc. 
  	sublocality_level_1: 'long_name', // locality 
  	administrative_area_level_1: 'long_name', // state
  	administrative_area_level_2: 'long_name', // city
    locality: 'long_name',
  	country: 'long_name',
    postal_code: 'short_name'
  };

  $scope.options = {
        componentRestrictions: {country: "in"},
        types: ['geocode']
  };


  $scope.$on('gmPlacesAutocomplete::placeChanged', function(){
      $scope.placeSelected = {};
      var place = $scope.entered_locality.getPlace();

        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
             $scope.placeSelected[addressType] = val;
          }
        }


        $scope.store.locality.name = place.name;
        if($scope.placeSelected['sublocality_level_2']){
          $scope.store.locality.name = $scope.placeSelected['sublocality_level_2'];
        }
        $scope.store.locality.city.name = $scope.placeSelected['administrative_area_level_2'];
        if($scope.placeSelected.locality){
          $scope.store.locality.city.name = $scope.placeSelected.locality;
        }
        $scope.store.locality.city.state = $scope.placeSelected['administrative_area_level_1'];
        $scope.store.locality.pincode = $scope.placeSelected['postal_code'];
        $scope.$apply();

      });


  $scope.update_store_details = function(){
    StoreService.update_store($scope.store).success(function(data){
      alert("Store details updated");
    })
  };

  $scope.show_add_stores = function(){
    $scope.store = {};
    $scope.store.locality = {};
    $scope.store.locality.city = {};
    $('#store_add_view').css('display','block');
    $('#stores_table').css('display','none');
    $('#store_view').css('display','none');  
  };


  $scope.add_store = function(){
    $scope.store.locality.cityId = city_to_id_map[$scope.store.locality.city.name]
    StoreService.create_store($scope.store,$scope.create_user).success(function(data){
      alert("Store addition success");
      $scope.go_to_store_details($scope.store);
    })
  };


  $scope.get_stores = function(){
    StoreService.get_stores($scope.store_query).success(function(data){
      $scope.stores = data; 
    })
  };

  $scope.get_sales_users = function(){
    StoreService.get_sales_users().success(function(data){
      $scope.sales_users = data;
    })
  }

  $scope.go_to_store_details = function(store){
    delete $scope.store;
    $scope.store = store;
    if($scope.store.locality){
      $scope.entered_locality = store.locality.name + 
                      "," + store.locality.city.name + "," + store.locality.city.state;
    }
    $('#store_view').css('display','block');
    $('#stores_table').css('display','none');
    $('#store_add_view').css('display','none');
  };

  $scope.back_to_stores = function(store){
    delete $scope.store;
    $('#stores_table').css('display','block');
    $('#store_view').css('display','none');
    $('#store_add_view').css('display','none');
  };

  StoreService.get_sales_users().success(function(data){
      $scope.sales_users = data;
  })
});