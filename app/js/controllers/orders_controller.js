angular.module("app").controller('OrdersController', function($scope, $location, $window, $localStorage, 
  $templateCache, OrdersSerivce, $q) {


var max_count = 100;
var count = 0;
var limit = 10;
var offset = 0;
var loading_orders = false;

$scope.freq_list = [];
$scope.freq_list_data_pivot = [];
$scope.kg_list = [];



   var order_query = {

      status: "RECEIVED,PROCESSING,PACKED,DISPATCHED,DELIVERED,RETURNED",
      city_id: 1,
      from_date: new Date(),
      to_date: new Date(),
      limit: 10,
      offset: count,
      max_count: max_count,
      selected_orders: 0
   }


   order_query.from_date.setHours("0");
   order_query.from_date.setMinutes("0");
   order_query.to_date.setHours("23");
   order_query.to_date.setMinutes("59");
   order_query.to_date.setSeconds("59");
   order_query.from_date.setSeconds("00");

   
   if($localStorage.order_query){
      $scope.order_query = $localStorage.order_query
      $scope.order_query.from_date = new Date($localStorage.order_query.from_date);
      $scope.order_query.to_date = new Date($localStorage.order_query.to_date);
      order_query = $scope.order_query;
   } else{
     $scope.order_query = order_query;
     $scope.orders = [];
     $scope.select_all = false;
     $localStorage.order_query = order_query;

   }

   $scope.orders = [];
   $scope.loading_orders = loading_orders;
   $scope.order_query.selected_orders = 0;

   
   
   if($localStorage.user == null){
     $location.path('/#/login');
     return;
   }

   $scope.get_next_orders_page = function(){

      try{

         if(max_count > count && !$scope.loading_orders){ 
            $scope.loading_orders = true;
            order_query.offset = count;
               OrdersSerivce.get_orders(order_query).success(function(next_page_orders){
                  if(next_page_orders.count > 0 ){
                     $scope.orders = $scope.orders.concat(next_page_orders.orders);
                     count = count + next_page_orders.orders.length;
                     max_count = next_page_orders.count; 
                     $scope.loading_orders =   false;
                  } else{
                     max_count = 0;
                     $scope.loading_orders = true;
                  }           
               })
            
         } else{
            $scope.loading_orders = true;
            return;
         }

      } catch(err){
         $scope.loading_orders = true;
      }

   }.bind(this);

   $scope.activate_order_search =  function(){
       if(order_query.from_date && order_query.to_date){  
         $scope.orders = [];
         $scope.loading_orders = false;
         max_count = 100000;
         $scope.order_query.selected_orders = 0;
         count = 0;
         offset = 0;
         $scope.select_all = false;
         $scope.$emit('query_again');
       } else {
          alert("Please Select a from and to time"); 
       }
   };

   $scope.go_to_order_details = function(order_id){
      $localStorage.order_query = $scope.order_query;
      $location.path('/order_details/'+order_id);
       // $window.open('/#/order_details/'+order_id);
       // return;
   };

   $scope.select_order = function(order){
      if(order.selected)
        $scope.order_query.selected_orders += 1;
      else
        $scope.order_query.selected_orders -= 1;
   };

   $scope.select_all_orders = function(){
      

      if($scope.select_all){
       for(index in $scope.orders){
          $scope.orders[index].selected = true;
       }
       $scope.order_query.selected_orders = $scope.orders.length;
     } else{
        for(index in $scope.orders){
          delete $scope.orders[index].selected;
       }
      $scope.order_query.selected_orders = 0;
     }

   };


   $scope.change_status = function(){

     var selected_order_ids = [];

     for(index in $scope.orders){
      if($scope.orders[index].selected)
        selected_order_ids .push($scope.orders[index].id);     
     }

     if(selected_order_ids.length == 0){
      alert("Please select atleast one order for changing status");
      return;
     }
     var selected_ids = selected_order_ids.toString();
     
     OrdersSerivce.update_status(selected_ids, order_query.status).success(function(){
        alert("Orders Status has been updated");
     });

      $scope.orders = [];
      $scope.loading_orders = false;
      $scope.order_query.selected_orders = 0;
      max_count = 100000;
      count = 0;
      $scope.select_all = false;
      $scope.$emit('query_again');

   };

   $scope.generate_procurement_list = function(){

      var selected_order_ids = [];
      $templateCache.removeAll();

     for(index in $scope.orders){
        if($scope.orders[index].selected){
          if($scope.orders[index].status != "RECEIVED"){
            alert("Only orders in received state are eligible for generating proc list");
            return;
          }
          selected_order_ids .push($scope.orders[index].id);
        }       
     }

     if(selected_order_ids.length == 0){
      alert("Please select atleast one order for generating proc list");
      return;
     }
     var selected_ids = selected_order_ids.toString();
     delete $localStorage.proc_list_str;

     OrdersSerivce.update_status(selected_ids, "PROCESSING").success(function(){
        $localStorage.proc_list_str = selected_ids;
        $window.open('/#/proc_list', 'C-Sharpcorner', 'width=500,height=400');   
     });
     
   };


   $scope.generate_frequency_list = function(){

      var selected_order_ids = [];
      var freq_list_data = null;
      $templateCache.removeAll();

     for(index in $scope.orders){
        if($scope.orders[index].selected){
          // if($scope.orders[index].status != "PROCESSING"){
          //   alert("Only orders in received state are eligible for generating proc list");
          //   return;
          // }
          selected_order_ids .push($scope.orders[index].id);
        }       
     }

     if(selected_order_ids.length == 0){
      alert("Please select atleast one order for generating frequency list");
      return;
     }

      var selected_ids = selected_order_ids.toString();
      OrdersSerivce.get_freq_list(selected_ids).success(function(data){
       $scope.freq_list_data = data;      
       $('#freq_list_modal').modal('show');
     });
  };

  $scope.get_frequency_list = function(){
      if(!$scope.freq_list_data){
        alert("Generate CSV first");
      }
      $scope.skuMap = new Map();
      $scope.kg_list = [];       
      $scope.sku_list = [];       
      $scope.kg_list[0]="Item Name";  
      $scope.sku_list[0] = "Item Name";
      for (var i = 0; i < $scope.freq_list_data.length; i++) {        
        $scope.kg_list[i+1] = $scope.freq_list_data[i][1];
        $scope.sku_list[i+1] = $scope.freq_list_data[i][0];
      }
      $scope.kg_list = unique($scope.kg_list);        
      $scope.sku_list = unique($scope.sku_list);

      $scope.skuMap.set("Item Name", $scope.kg_list);
      var index = 0;
      for (var i = 0; i < $scope.freq_list_data.length; i++) {
        var tempArray = [];
        for(var j=0; j<$scope.kg_list.length;j++){
          if(j == 0){
            tempArray[j] = $scope.freq_list_data[i][0];
          }else{
            if($scope.kg_list[j] == $scope.freq_list_data[i][1]){
              tempArray[j] = $scope.freq_list_data[i][2];
            }else{
              tempArray[j] = "";
            }
          }  
        }
  
        if(!$scope.skuMap.has(tempArray[0])){
          $scope.skuMap.set(tempArray[0],tempArray);                
        }else{
          var mergeArray = $scope.skuMap.get(tempArray[0]);
          for(var index = 1;index < mergeArray.length; index++){
            if(mergeArray[index]==""){
              mergeArray[index] = tempArray[index];
            }
          }
          $scope.skuMap.set(tempArray[0],mergeArray);        
        }        
      }

      for(var i = 0;i<$scope.skuMap.size;i++){
          $scope.freq_list_data_pivot.push($scope.skuMap.get($scope.sku_list[i]));
      }

      data = $scope.freq_list_data_pivot;    
      delete $scope.freq_list_data;
      delete $scope.kg_list
      delete $scope.sku_list;
      delete $scope.freq_list_data_pivot;     
      delete $scope.skuMap;     
      return data;
   };

  // $scope.get_header = function(){
  //   $scope.kg_list = [];          
  //   $scope.kg_list[0]="Item Name"  
  //   for (var i = 0; i < $scope.freq_list_data.length; i++) {        
  //     $scope.kg_list[i+1] = $scope.freq_list_data[i][1];
  //   }
  //   $scope.kg_list = unique($scope.kg_list);
  //   data = $scope.kg_list;
  //   delete $scope.freq_list_data;
  //   delete $scope.kg_list;
  //   delete $scope.freq_list_data_pivot;     
  //   return data;    
  // };
  
  var unique = function(origArr) {
    var newArr = [],
        origLen = origArr.length,
        found, x, y;

    for (x = 0; x < origLen; x++) {
        found = undefined;
        for (y = 0; y < newArr.length; y++) {
            if (origArr[x] === newArr[y]) {
                found = true;
                break;
            }
        }
        if (!found) {
            newArr.push(origArr[x]);
        }
    }
    return newArr;
  };

  // ['Item name','Quantity','Frequency']

  // if($scope.freq_list_data[i][0] != $scope.freq_list_data[i+1][0]){
  //           index++;
  //           alert(index);
  //         } 

   // $scope.generate_procurement_list = function(){

   //   var selected_orders = [];

   //   for(index in $scope.orders){

   //      if($scope.orders[index].selected)
   //        selected_orders .push($scope.orders[index]);     
   //   }

   //   if(selected_orders.length == 0){
   //    alert("Please select atleast one order for changing status");
   //    return;
   //   }

   //   var proc_list = {};
     
   //   var orders = $scope.orders
   //   for(index1 in orders){
   //       var orderitems = orders[index1].orderItems;
   //       for(index2 in orderitems){
   //        var product = orderitems[index2].product;
   //        var productUnit = orderitems[index2].productUnit;
   //           if(proc_list[product.id] == null){
   //              proc_list[product.id] = angular.copy(product);
   //              proc_list[product.id].quantity = 0; 
   //           }
   //           proc_list[product.id].quantity = proc_list[product.id].quantity + 
   //                           (productUnit.quantity * orderitems[index2].quantity);
   //           proc_list[product.id].unit =  orderitems[index2].productUnit;               
   //       }
   //   }
   //   OrdersSerivce.set_proc_list(proc_list);
   //   $location.path('/proc_list');
     
   // };
   

});
