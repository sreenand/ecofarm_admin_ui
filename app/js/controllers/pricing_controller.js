angular.module("app").controller('PricingController', function($scope, $location, $window, $localStorage, ProductService){


   var pricing_query = {};
   pricing_query.city_id = 1;

   $scope.pricing_list = {};
   $scope.sub_categories = {};

    $scope.set_sub_category =  function(sub_category){
        pricing_query.sub_cat_id = sub_category.id;
    };

   	$scope.get_sub_categories = function(){
   		delete $scope.sub_categories
   		ProductService.get_all_subcategories(pricing_query.category_id).success(function(data){
   			$scope.sub_categories = data;
   		});
   	};

   	$scope.get_pricing = function(){
   		var pricing_query = $scope.pricing_query;
   		if(!pricing_query.city_id || !pricing_query.sub_cat_id){
   			alert("Select a city , Category, Sub Category to get prices");
   			return;
   		}
   		ProductService.get_prices(pricing_query).success(function(data){
   			$scope.pricing_list = data;
   		})
   	};

   	$scope.update_pricing = function(pricing){
     
        ProductService.update_pricing(pricing).success(function(data){
   			alert("updated "+pricing.product.name);
   		})
   	};


});