angular.module("app").controller('LoginController', function($scope, $location, $localStorage, AuthenticationService) {

   if($localStorage.user != null){
     $location.path('/home');
   }

  $scope.credentials = { username: "", password: "" };

  var onLoginSuccess = function(user) {

    if(user.type === 'ADMIN' || user.type === 'WAREHOUSE_ADMIN'){
      $localStorage.user = user;
      $location.path('/home');
    } else{
      alert("You do not have permission to operate this panel");
    }
    
  };

  $scope.login = function() {
    AuthenticationService.login($scope.credentials).success(onLoginSuccess);
  };


  $scope.logout = function() {
    delete $localStorage.user;
    $location.path('/login');
  };
});
