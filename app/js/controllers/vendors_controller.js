angular.module("app").controller('VendorsController', function($scope, $location, $window, 
	$localStorage, $templateCache, VendorService, $q) {


  var placeSearch, autocomplete;
  $scope.vendors = {};
  $scope.vendor_transactions = {};
  $scope.markets = {};

  $scope.get_stores = function(){
    // StoreService.get_stores($scope.vendors).success(function(data){
    //   $scope.stores = data; 
    // })
  };

  $scope.addVendor = function(vendor_name, vendor_market, vendor_owner, vendor_contact){
      if(vendor_name != null && vendor_market != null && vendor_owner != null && vendor_contact != null){
          var vendor = new Object();
          vendor.vendor_name = vendor_name;
          vendor.sourcing_mkt_id = vendor_market;
          vendor.owner_name = vendor_owner;
          vendor.contact_number = vendor_contact;
          VendorService.add_vendor(vendor).success(function(data){
            alert("New Vendor Added successfully")
            $window.location.reload();          
          })
      }else{
        alert("Please fill in the details properly!!!")
      }
  };

  $scope.getVendorTransactions = function(vendor_id){
    VendorService.get_vendor_transactions(vendor_id).success(function(data){
      $scope.vendor_transactions = data;
    })
  }
  VendorService.get_vendors().success(function(data){
      $scope.vendors = data; 
  })

  VendorService.get_markets().success(function(data){
      $scope.markets = data; 
  })
});