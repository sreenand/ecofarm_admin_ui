angular.module("app").controller('UserController', function($scope, $location,$window, 
	$localStorage, UserService) {


    if($localStorage.user == null){
     $location.path('/login');
    }


	$scope.users = [];
	$scope.selectedUser = {};
	$scope.user_query = {};

    $scope.get_users = function (){
    	$scope.users = [];
    	UserService.get_users($scope.user_query.mobile_no).success(function(user){
    		$scope.users.push(user);
    	})
    };

    $scope.go_to_user_details = function (user){
    	delete $scope.selectedUser;
    	$scope.selectedUser = user;
        $('#user_view').modal('show');
    };

    $scope.show_add_user_page = function (user){
        $scope.selectedUser = {};
        $('#user_add').modal('show');
    };

    $scope.update_user = function(){
        UserService.update_user($scope.selectedUser).success(function(data){
          alert("User updated successfully");
        })
    };

    $scope.collect_cash = function(){
        UserService.collect_cash($scope.selectedUser).success(function(data){
          $scope.get_users();
        })
    };

    $scope.add_user = function(){
        UserService.add_user($scope.selectedUser).success(function(data){
          $('#user_add').modal('hide');
          $scope.user_query.mobile_no = $scope.selectedUser.phoneNo;
          $scope.get_users();
        })
    };


});