angular.module("app").config(function($routeProvider, $locationProvider) {

  //$locationProvider.html5Mode({enabled:true});

  $routeProvider.when('/login', {
    templateUrl: 'login.html',
    controller: 'LoginController'
  });

  $routeProvider.when('/home', {
    templateUrl: 'home.html',
    controller: 'HomeController'
  });

  $routeProvider.when('/orders', {
    templateUrl: 'orders.html',
    controller: 'OrdersController'
  });

  $routeProvider.when('/order_details/:order_id', {
    templateUrl: 'order_details.html',
    controller: 'OrderDetailsController'
  });

  $routeProvider.when('/orders/:order_id/invoice', {
    templateUrl: 'order_invoice.html',
    controller: 'OrderInvoiceController'
  });

  $routeProvider.when('/proc_list', {
    templateUrl: 'proc_list.html',
    controller: 'MiscController'
  });

  $routeProvider.when('/stores', {
    templateUrl: 'stores.html',
    controller: 'StoresController'
  });

  $routeProvider.when('/pricing', {
    templateUrl: 'pricing.html',
    controller: 'PricingController'
  });

  $routeProvider.when('/users', {
    templateUrl: 'users.html',
    controller: 'UserController'
  });

  $routeProvider.when('/user_details', {
    templateUrl: 'user_view.html',
    controller: 'UserController'
  });

  $routeProvider.when('/procurement', {
    templateUrl: 'procurement.html',
    controller: 'ProcurementController'
  });

  $routeProvider.when('/procurementorders', {
    templateUrl: 'proc_orders.html',
    controller: 'ProcurementOrdersController'
  });

  $routeProvider.when('/purchase_order_details/:order_id', {
    templateUrl: 'proc_order_details.html',
    controller: 'ProcOrderDetailsController'
  });

  $routeProvider.when('/vendors', {
    templateUrl: 'vendors.html',
    controller: 'VendorsController'
  });

  $routeProvider.otherwise({ redirectTo: '/home' });

});
