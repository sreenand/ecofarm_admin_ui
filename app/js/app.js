angular.module("app", ["ngResource", "ngRoute", 'ngCookies', "ngStorage", "infinite-scroll",  "ngSanitize", "ngCsv","gm"]).run(function($rootScope) {
  // adds some basic utilities to the $rootScope for debugging purposes
  $rootScope.log = function(thing) {
    console.log(thing);
  };

  $rootScope.alert = function(thing) {
    alert(thing);
  };
});
angular.module("app").config(function ($httpProvider) {
    $httpProvider.defaults.withCredentials = true;
    //rest of route code
});