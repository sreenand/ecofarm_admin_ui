angular.module("app").factory('StoreService', ['$http','$location', '$localStorage', 'ConfigurationService', 
  function($http, $location, $localStorage, ConfigurationService) {


  	return {
  		get_stores: function(store_query){
  			return $http({
      	    "url": ConfigurationService.stores_url + "/all" ,
      	    "method": "GET",
      	    "headers" : {"content-Type": "application/json"},
      	    "params" :store_query
         }).success(function (data, status, headers, config) {
              return data;
         }).error(function (data, status, header, config) {
             ConfigurationService.handle_errors(status);
         });
  	   },

       update_store: function(store){
        return $http({
            "url": ConfigurationService.stores_url + "/" + store.id,
            "method": "PUT",
            "headers" : {"content-Type": "application/json"},
            "data" :angular.toJson(store)
         }).success(function (data, status, headers, config) {
              return data;
         }).error(function (data, status, header, config) {
             ConfigurationService.handle_errors(status);
         });
       },

       create_store: function(store, createUser){
        delete store.locality.city;
        return $http({
            "url": ConfigurationService.stores_url + "?create_user=" + createUser,
            "method": "POST",
            "headers" : {"content-Type": "application/json"},
            "data" :angular.toJson(store)
         }).success(function (data, status, headers, config) {
              return data;
         }).error(function (data, status, header, config) {
             ConfigurationService.handle_errors(status);
         });
       },

       get_sales_users: function(store_query){
        return $http({
            "url": ConfigurationService.users_url + "/get_sales_users" ,
            "method": "GET",
            "headers" : {"content-Type": "application/json"},
            "params" :store_query
         }).success(function (data, status, headers, config) {
              return data;
         }).error(function (data, status, header, config) {
             ConfigurationService.handle_errors(status);
         });
       },
  	}


 }]);