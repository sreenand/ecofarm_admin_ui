angular.module("app").factory('VendorService', ['$http','$location', '$localStorage', 'ConfigurationService', 
  function($http, $location, $localStorage, ConfigurationService) {


  	return {
  		get_vendors: function(){
  			return $http({
      	    "url": ConfigurationService.vendors_get_url,
      	    "method": "GET",
      	    "headers" : {"content-Type": "application/json"},      	   
         }).success(function (data, status, headers, config) {
              return data;
         }).error(function (data, status, header, config) {
             ConfigurationService.handle_errors(status);
         });
  	   },

       get_markets: function(){
        return $http({
            "url": ConfigurationService.get_markets_url,
            "method": "GET",
            "headers" : {"content-Type": "application/json"},          
         }).success(function (data, status, headers, config) {
              return data;
         }).error(function (data, status, header, config) {
             ConfigurationService.handle_errors(status);
         });
       },

       add_vendor: function(vendor){
          var vendor_copy = {}; 
          vendor_copy = angular.copy(vendor);
          return $http({
            "url": ConfigurationService.add_vendor_url,
            "method": "POST",
            "headers" : {"content-Type": "application/json"},          
            "data": JSON.stringify(vendor_copy)
         }).success(function (data, status, headers, config) {
              return data;
         }).error(function (data, status, header, config) {
             ConfigurationService.handle_errors(status);
         });
       },

       get_vendor_transactions: function(vendor_id){
        return $http({
            "url": ConfigurationService.get_vendor_transactions+"/"+vendor_id,
            "method": "GET",
            "headers" : {"content-Type": "application/json"},          
         }).success(function (data, status, headers, config) {
              return data;
         }).error(function (data, status, header, config) {
             ConfigurationService.handle_errors(status);
         });
       },
  	}
 }]);