angular.module("app").factory('UserService', ['$http','$location', '$localStorage', 'ConfigurationService', 
  function($http, $location, $localStorage, ConfigurationService) {
  // these routes map to stubbed API endpoints in config/server.js
  return {
    get_users: function(mobile_no) {
      return $http({
      	"url": ConfigurationService.user_find_url + "/" + mobile_no,
      	"method": "GET",
      	"headers" : {"content-Type": "application/json"}
      }).success(function (data, status, headers, config) {
              delete data.password;
      	      return data;
      }).error(function (data, status, header, config) {
             ConfigurationService.handle_errors(status);
      });
    },

    add_user: function(user) {

      return $http({
        "url": ConfigurationService.users_url + "/register",
        "method": "POST",
        "headers" : {"content-Type": "application/json"},
        "data": angular.toJson(user)
      }).success(function (data, status, headers, config) {
            alert("User Created!!!!")
      }).error(function (data, status, header, config) {
             ConfigurationService.handle_errors(status);
      });
    },

    update_user: function(user) {
      return $http({
        "url": ConfigurationService.users_url,
        "method": "PUT",
        "headers" : {"content-Type": "application/json"},
        "data": angular.toJson(user)
      }).success(function (data, status, headers, config) {
            user.userWallet = wallet;
      }).error(function (data, status, header, config) {
             ConfigurationService.handle_errors(status);
      });
    },


    collect_cash: function(user) {
      return $http({
        "url": ConfigurationService.users_url + "/" + user.id + "/wallet/remit?city_id=1",
        "method": "PUT",
        "headers" : {"content-Type": "application/json"},
        "data":{}
      }).success(function (data, status, headers, config) {
         alert("Collected " + user.userWallet.currentBalance + "Rs");
      }).error(function (data, status, header, config) {
             ConfigurationService.handle_errors(status);
      });
    },

    reset_password: function(user,newPassword){
      return $http({
        "url": ConfigurationService.users_url + "/password/reset/" + user.id,
        "method": "PUT",
        "headers" : {"content-Type": "application/json"},
        "data": {"password":newPassword},
      }).success(function (data, status, headers, config) {
              return data;
      }).error(function (data, status, header, config) {
             ConfigurationService.handle_errors(status);
      });
    }
  };
  
}]);
