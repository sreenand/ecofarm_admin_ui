angular.module("app").factory('AuthenticationService', ['$http','$location', '$localStorage', 'ConfigurationService', function($http, $location, $localStorage, ConfigurationService) {
  // these routes map to stubbed API endpoints in config/server.js
  return {
    login: function(credentials) {
      return $http({
      	"url": ConfigurationService.user_login_url,
      	"method": "POST",
      	"data": JSON.stringify(credentials),
      	"headers" : {"content-Type": "application/json"}
      }).success(function (data, status, headers, config) {
      	      return data;
      }).error(function (data, status, header, config) {
             ConfigurationService.handle_errors(status);
      });
    },

    logout: function() {
      delete $localStorage.user;
      $location.path('/login');
    }
  };
  
}]);
