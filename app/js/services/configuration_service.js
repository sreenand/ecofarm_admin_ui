angular.module("app").factory('ConfigurationService', function($location, $localStorage) {
  var backend_url = "http://54.169.106.64:8080";
    // var backend_url = "http://127.0.0.1:8080";
  
  return {
     user_login_url: backend_url + "/users/login",
     user_logout_url: backend_url + "/users/logout",
     user_find_url: backend_url + "/users/find",
     stores_url: backend_url + "/stores",
     users_url: backend_url + "/users",
     orders_url: backend_url + "/orders",
     order_details_url: backend_url + "/orders",
     products_root_url: backend_url + "/sku",
     pricing_get_url: backend_url+ "/sku/query",
     category_get_url: backend_url+ "/sku/category",
     markets_get_url: backend_url+ "/markets/getall",
     vendors_get_url: backend_url+ "/vendors",
     cart_add_url: backend_url+ "/purchasecart/add",
     cart_remove_url: backend_url+ "/purchasecart/remove",
     get_all_cart_url: backend_url+ "/purchasecart/getall",
     purchase_orders_url: backend_url+ "/purchase_orders",
     purchase_order_item_url: backend_url+ "/purchase_order_item",
     place_purchase_order_url: backend_url+ "/purchase_orders/createorder",
     get_purchase_order_url: backend_url+ "/purchase_orders/getAll",
     get_markets_url: backend_url+ "/markets/getall",
     add_vendor_url: backend_url+ "/vendors/add",
     get_vendor_transactions: backend_url+ "/vendor_transaction",

     handle_errors: function(status){

         if(status === 401){
            alert("Session Timed out");
            delete $localStorage.user;
            $location.path("/login");            
         } else if(status === 404){
            alert(" requested resource is not found");
            throw 404;
         } else {
         	alert(" Something went wrong terribly");
            throw 500;
         }
     } 
  };
});
