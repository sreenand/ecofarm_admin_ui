angular.module("app").factory('ProductService', ['$http','$location', '$localStorage', 'ConfigurationService', function($http, $location, $localStorage, ConfigurationService){


	return {

		get_prices: function(pricing_query){

	     return $http({
	      	"url": ConfigurationService.pricing_get_url,
	      	"method": "GET",
	      	"headers" : {"content-Type": "application/json"},
	      	"params": {"city_id":pricing_query.city_id, "sub_cat_id":pricing_query.sub_cat_id}
	      }).success(function (data, status, headers, config) {
	      	      return data;
	      }).error(function (data, status, header, config) {
	             ConfigurationService.handle_errors(status);
	      });

		},

		get_all_subcategories: function(category_id){

			return $http({
	      	"url": ConfigurationService.category_get_url+"/1/subcategories",
	      	"method": "GET",
	      	"headers" : {"content-Type": "application/json"}
	      }).success(function (data, status, headers, config) {
	      	      return data;
	      }).error(function (data, status, header, config) {
	             ConfigurationService.handle_errors(status);
	      });

		},

		update_pricing: function(pricing){

            var pricing_update_request = {}; 
            pricing_update_request = angular.copy(pricing);
            delete pricing_update_request.product;
            delete pricing_update_request.productUnit;
			return $http({
	      	"url": ConfigurationService.products_root_url+"/pricing",
	      	"method": "PUT",
	      	"headers" : {"content-Type": "application/json"},
	      	"data": JSON.stringify(pricing_update_request)
	      	
	      }).success(function (data, status, headers, config) {
	      	      return data;
	      }).error(function (data, status, header, config) {
	             ConfigurationService.handle_errors(status);
	      });
		},

		get_all_markets: function(){
			return $http({
	      	"url": ConfigurationService.markets_get_url,
	      	"method": "GET",
	      	"headers" : {"content-Type": "application/json"}
	      }).success(function (data, status, headers, config) {
	      	      return data;
	      }).error(function (data, status, header, config) {
	             ConfigurationService.handle_errors(status);
	      });
		},

		get_all_vendors: function(marketSelected){
			return $http({
	      	"url": ConfigurationService.vendors_get_url+"/getall",
	      	"method": "GET",
	      	"headers" : {"content-Type": "application/json"},
	      	"params": {"marketSelected":marketSelected}
	      }).success(function (data, status, headers, config) {
	      	      return data;
	      }).error(function (data, status, header, config) {
	             ConfigurationService.handle_errors(status);
	      });
		},

		addToCart: function(cart){
			var cart_insert_request = {}; 
            cart_insert_request = angular.copy(cart);           
			return $http({
	      	"url": ConfigurationService.cart_add_url,
	      	"method": "POST",
	      	"headers" : {"content-Type": "application/json"},
	      	"data": JSON.stringify(cart_insert_request)	      	
	      }).success(function (data, status, headers, config) {
	      	      return data;
	      }).error(function (data, status, header, config) {
	             ConfigurationService.handle_errors(status);
	      });
		},

		getAllCart: function(){			                   
			return $http({
	      	"url": ConfigurationService.get_all_cart_url,
	      	"method": "GET",
	      	"headers" : {"content-Type": "application/json"},	      
	      }).success(function (data, status, headers, config) {
	      	      return data;
	      }).error(function (data, status, header, config) {
	             ConfigurationService.handle_errors(status);
	      });
		},

		placeProcurementOrder: function(cartList){
			var cart_request = {};    
			cart_request = angular.copy(cartList)                 
			return $http({
	      	"url": ConfigurationService.place_purchase_order_url,
	      	"method": "POST",
	      	"headers" : {"content-Type": "application/json"},
	      	"data": JSON.stringify(cart_request)	      	
	      }).success(function (data, status, headers, config) {
	      	      return data;
	      }).error(function (data, status, header, config) {
	             ConfigurationService.handle_errors(status);
	      });
		},

		removeFromCart: function(cart){
			var cart_insert_request = {}; 
            cart_insert_request = angular.copy(cart);           
			return $http({
	      	"url": ConfigurationService.cart_remove_url,
	      	"method": "POST",
	      	"headers" : {"content-Type": "application/json"},
	      	"data": JSON.stringify(cart_insert_request)	      	
	      }).success(function (data, status, headers, config) {
	      	      return data;
	      }).error(function (data, status, header, config) {
	             ConfigurationService.handle_errors(status);
	      });
		}		
	}

}]);