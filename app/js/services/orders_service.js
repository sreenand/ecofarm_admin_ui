angular.module("app").factory('OrdersSerivce', ['$http','ConfigurationService', function($http, ConfigurationService) {

  return {

    
    get_orders: function(payload){
      return $http({
        "url": ConfigurationService.orders_url,
        "method": "GET",
        "headers": {"content-Type": "application/json"},
        "params": {city_id: payload.city_id,
                status: payload.status,
                from_date: payload.from_date.toJSONLocal(),
                to_date: payload.to_date.toJSONLocal(),
                limit: payload.limit,
                offset: payload.offset }
      }).success(function (data, status, headers, config) {
              return data;
      }).error(function (data, status, header, config) {
             ConfigurationService.handle_errors(status);
      });
    },

    get_order_by_id: function(order_id){

      return $http({
        "url": ConfigurationService.orders_url + "/" + order_id,
        "method": "GET",
        "headers": {"content-Type": "application/json"}
      }).success(function (data, status, headers, config) {
              return data;
      }).error(function (data, status, header, config) {
             ConfigurationService.handle_errors(status);
      });
    },


    populate_prices: function(order){

      return $http({
        "url": ConfigurationService.orders_url + "/" + JSON.stringify(order.id) + "/orderitems?populate_price=true",
        "method": "GET",
        "headers": {"content-Type": "application/json"}
      }).success(function (data, status, headers, config) {
              return data;
      }).error(function (data, status, header, config) {
             ConfigurationService.handle_errors(status);
      });
    },

    update_order: function(order){

      return $http({
        "url": ConfigurationService.orders_url + "/",
        "method": "PUT",
        "headers": {"content-Type": "application/json"},
        "data" : angular.toJson(order)
      }).success(function (data, status, headers, config) {
              return data;
      }).error(function (data, status, header, config) {
             ConfigurationService.handle_errors(status);
      });

    },

    update_status: function(order_id_list_string, status_to_be_updated){

      return $http({
        "url": ConfigurationService.orders_url + "/" + order_id_list_string + "/status/" + status_to_be_updated,
        "method": "PUT",
        "headers": {"content-Type": "application/json"}
      }).success(function (data, status, headers, config) {
              alert("Order status has been updated Successfully");
      }).error(function (data, status, header, config) {
             ConfigurationService.handle_errors(status);
      });

    },

    get_proc_list : function(proc_list_str){

       return $http({
        "url": ConfigurationService.orders_url + "/" + proc_list_str + "/proc_list",
        "method": "GET",
        "headers": {"content-Type": "application/json"}
      }).success(function (data, status, headers, config) {
              return data;
      }).error(function (data, status, header, config) {
             ConfigurationService.handle_errors(status);
      });
       
    },  


    get_freq_list: function(freq_list_str){

      return $http({
        "url": ConfigurationService.orders_url + "/" + freq_list_str + "/freq_list",
        "method": "GET",
        "headers": {"content-Type": "application/json"}
      }).success(function (data, status, headers, config) {
              return data;
      }).error(function (data, status, header, config) {
             ConfigurationService.handle_errors(status);
      });

    }

  };


}]);

Date.prototype.toJSONLocal = (function() {
    return function() {
      var d = new Date(this.getTime() + 19800000);
      return d.toJSON();
    };
}())

